package diningphilosophers;

import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author Philip
 */
public class PhilosopherMain {

    public static void main(String[] args) throws InterruptedException {
        DiningPhilosophers[] philosophers = new DiningPhilosophers[5];
        ReentrantLock table = new ReentrantLock();

        for (int i = 0; i < 5; ++i) {
            philosophers[i] = new DiningPhilosophers(table);
        }
        for (int i = 0; i < 5; ++i) {
            philosophers[i].setRightNeighbour(philosophers[(i + 1) % 5]);
            philosophers[i].setLeftNeighbour(philosophers[(i + 4) % 5]);
            philosophers[i].start();
        }

        for (int i = 0; i < 5; ++i) {
            philosophers[i].join();
        }

    }
}
