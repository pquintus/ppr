package diningphilosophers;

import java.util.Random;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author Philip
 */
class DiningPhilosophers extends Thread {

    private boolean eating;
    private final Random random;
    private int thinkCount;
    private DiningPhilosophers leftNeighbour;
    private DiningPhilosophers rightNeighbour;
    ReentrantLock table;
    private final Condition condition;

    /**
     * Kann es mit Bedingungsvariablen zu Verklemmungen kommen? (bitte als
     * kurzen Kommentar in den Quelltext)
     *
     * Es kann zu Verklemmungen kommen, wenn die Bedingungen kein signal
     * bekommen und immer nur im await stecken. Durch die signals am Anfang der
     * Schleife, wenn die Philosophen denken, wird dieser Effekt verhindert.
     */
    public DiningPhilosophers(ReentrantLock table) {
        this.random = new Random();
        this.table = table;
        this.condition = table.newCondition();
        eating = false;
    }

    @Override
    public void run() {
        while (true) {
            //thinking
            table.lock();
            try {
                eating = false;
                rightNeighbour.condition.signal();
                leftNeighbour.condition.signal();
            } finally {
                table.unlock();
            }
            ++thinkCount;
            if (thinkCount % 10 == 0) {
                System.out.println("Philosopher " + this + " has thought " + thinkCount + " times");
            }
            try {
                Thread.sleep(random.nextInt(1000));
            } catch (InterruptedException ex) {
            } // Think for a while

            //eating
            table.lock();
            try {
                while (rightNeighbour.isEating() || leftNeighbour.isEating()) {
                    condition.await();
                }
            } catch (InterruptedException e) {
            } finally {
                table.unlock();
            }
            System.out.println("Philosopher " + this + " has eaten");
            eating = true;
            try {
                Thread.sleep(random.nextInt(1000));
            } catch (InterruptedException ex) {
            }// Eat for a while
        }
    }

    public void setLeftNeighbour(DiningPhilosophers leftNeighbour) {
        this.leftNeighbour = leftNeighbour;
    }

    public void setRightNeighbour(DiningPhilosophers rightNeighbour) {
        this.rightNeighbour = rightNeighbour;
    }

    public boolean isEating() {
        return eating;
    }

}
